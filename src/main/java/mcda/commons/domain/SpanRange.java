/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author kuba
 */
public class SpanRange implements Comparable<SpanRange> {

    private final int frameFrom;
    private final int frameTo;

    public SpanRange(int frameFrom, int frameTo) {
        if (frameFrom <= frameTo) {
            this.frameFrom = frameFrom;
            this.frameTo = frameTo;
        } else {
            this.frameFrom = frameTo;
            this.frameTo = frameFrom;
        }
    }

    public int getFrameFrom() {
        return frameFrom;
    }

    public int getFrameTo() {
        return frameTo;
    }

    @Override
    public int compareTo(SpanRange o) {
        return frameFrom - o.frameFrom;
    }

    public static List<SpanRange> complement(List<SpanRange> spanRanges, int from, int to) {
        List<SpanRange> tmp = new ArrayList<>(spanRanges);
        Collections.sort(tmp);
        List<SpanRange> result = new ArrayList<>();

        int start = from;
        for (SpanRange sr : tmp) {
            if (sr.getFrameTo() < start) {
                continue;
            }
            if (sr.getFrameFrom() > start) {
                if (sr.getFrameFrom() > to) {
                    break;
                } else {
                    result.add(new SpanRange(start, sr.getFrameFrom()));
                }
            }
            start = sr.getFrameTo();
        }
        if (start < to) {
            result.add(new SpanRange(start, to));
        }
        return result;
    }

    public static Set<SpanRange> spanIntersect(Collection<SpanRange> span1, Collection<SpanRange> span2) {
        return spanIntersect(span1, span2, 0);
    }

    public static Set<SpanRange> spanIntersect(Collection<SpanRange> span1, Collection<SpanRange> span2, int minLength) {

        List<SpanRange> in1 = new ArrayList<>(span1);
        List<SpanRange> in2 = new ArrayList<>(span2);

        Collections.sort(in1);
        Collections.sort(in2);

        Set<SpanRange> result = new HashSet<>();

        for (SpanRange in : in1) {

            for (int i = 0; i < in2.size() && in2.get(i).getFrameFrom() + minLength < in.getFrameTo(); i++) {
                if (in2.get(i).getFrameTo() - minLength < in.getFrameFrom()) {
                    continue;
                }
                result.add(new SpanRange(Math.max(in.getFrameFrom(), in2.get(i).getFrameFrom()), Math.min(in.getFrameTo(), in2.get(i).getFrameTo())));
            }

        }
        return result;
    }
}
