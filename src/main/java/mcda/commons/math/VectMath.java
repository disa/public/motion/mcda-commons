/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.math;

/**
 *
 * @author xvalcik
 */
public class VectMath {

    /**
     *
     *
     * @param joint1
     * @param joint2
     * @param joint3
     * @return [a,b,c,d]
     */
    public static final int DIMENSION = 3;

    public static double[] calculatePlane(double[] orthoVector, double[] point) {
        double[] normalizedNormVector = normalize(orthoVector);

        double d = -(normalizedNormVector[0] * point[0] + normalizedNormVector[1] * point[1] + normalizedNormVector[2] * point[2]);

        double[] plane = new double[DIMENSION + 1];
        System.arraycopy(normalizedNormVector, 0, plane, 0, DIMENSION);

        plane[DIMENSION] = d;
        return plane;
    }

    public static double[] crossProduct3D(double[] vect1, double[] vect2) {
        double[] crossProduct = new double[DIMENSION];
        crossProduct[0] = vect1[1] * vect2[2] - vect1[2] * vect2[1];
        crossProduct[1] = vect1[2] * vect2[0] - vect1[0] * vect2[2];
        crossProduct[2] = vect1[0] * vect2[1] - vect1[1] * vect2[0];
        return crossProduct;
    }

    public static double dotProduct3D(double[] vect1, double[] vect2) {
        double result = 0;
        for (int i = 0; i < DIMENSION; i++) {
            result += vect1[i] * vect2[i];
        }
        return result;
    }

    public static double[] normalize(double[] v) {
        return div3D(v, size3D(v));
    }

    public static double size3DSquare(double[] vect) {
        return vect[0] * vect[0] + vect[1] * vect[1] + vect[2] * vect[2];
    }

    public static double size3D(double[] vect) {
        return Math.sqrt(size3DSquare(vect));
    }

    public static double[] div3D(double[] vect, double div) {
        double[] result = new double[DIMENSION];
        for (int i = 0; i < DIMENSION; i++) {
            result[i] = vect[i] / div;
        }
        return result;
    }

    public static double[] subtract(double[] vectA, double[] vectB) {
        double[] result = new double[DIMENSION];
        for (int i = 0; i < DIMENSION; i++) {
            result[i] = vectA[i] - vectB[i];
        }
        return result;
    }

    public static double[] mul3D(double[] vect, double mul) {
        double[] result = new double[DIMENSION];
        for (int i = 0; i < DIMENSION; i++) {
            result[i] = vect[i] * mul;
        }
        return result;
    }

    public static double distance(double[] vect1, double[] vect2) {
        double result = 0;
        for (int i = 0; i < DIMENSION; i++) {
            result += (vect1[i] - vect2[i]) * (vect1[i] - vect2[i]);
        }
        return Math.sqrt(result);
    }

    public static float distance(float[] vect1, float[] vect2) {
        float result = 0;
        for (int i = 0; i < DIMENSION; i++) {
            result += (vect1[i] - vect2[i]) * (vect1[i] - vect2[i]);
        }
        return (float) Math.sqrt(result);
    }

    public static double[] projectPointOntoPlane(double[] point, double[] plane) {
        double[] result = new double[DIMENSION];
        double tmp = (dotProduct3D(point, plane) + plane[3]) / (plane[0] * plane[0] + plane[1] * plane[1] + plane[2] * plane[2]);
        for (int i = 0; i < DIMENSION; i++) {
            result[i] = point[i] - plane[i] * tmp;
        }
        return result;
    }

    /**
     * http://mathworld.wolfram.com/Line-LineIntersection.html
     *
     * @param a
     * @param b
     * @param x
     * @param y
     * @return
     */
    public static double[] calculateIntersection(double[] a, double[] b, double[] x, double[] y) {
        double ab[] = new double[DIMENSION];
        for (int i = 0; i < DIMENSION; i++) {
            ab[i] = b[i] - a[i];
        }

        double xy[] = new double[DIMENSION];
        for (int i = 0; i < DIMENSION; i++) {
            xy[i] = y[i] - x[i];
        }

        double ax[] = new double[DIMENSION];
        for (int i = 0; i < DIMENSION; i++) {
            ax[i] = x[i] - a[i];
        }

        if (dotProduct3D(ax, crossProduct3D(ab, xy)) > 1e-3) // lines are not coplanar
        {
            throw new IllegalStateException("lines are not coplanar");
        }

        double s = dotProduct3D(crossProduct3D(ax, xy), crossProduct3D(ab, xy)) / size3DSquare(crossProduct3D(ab, xy));

        double result[] = new double[DIMENSION];
        double tmp[] = mul3D(ab, s);
        for (int i = 0; i < DIMENSION; i++) {
            result[i] = a[i] + tmp[i];
        }
        return result;

    }
}
