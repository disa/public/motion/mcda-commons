/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.math;

/**
 *
 * @author xvalcik
 */
public class TimeSeries {

    public static float[] differenciate(float[] input) {
        float[] result = new float[input.length];
        System.arraycopy(input, 0, result, 0, input.length);
        for (int i = 0; i < result.length - 1; i++) {
            result[i] = result[i + 1] - result[i];
        }
        result[result.length - 1] = result[result.length - 2];
        return result;
    }
}
