/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.extraction.joint;

import mcda.commons.math.VectMath;

/**
 *
 * @author xvalcik
 */
public class SmoothedJointSpeed {

    private JointProvider jointProvider;

    private final static int K_MAX = 6;

    public float computeDispatchingValue(int frameNo) {
        return calculateNoiseReductedVelocity(frameNo);
    }

    private float getOneFrameVelocity(int frameNo) {
        assert frameNo >= 0;

        double[] j1, j2;
        if (frameNo < jointProvider.getFrameCount() - 1) {
            j1 = jointProvider.getFirstJoint(frameNo);
            j2 = jointProvider.getFirstJoint(frameNo + 1);
        } else {
            j1 = jointProvider.getFirstJoint(jointProvider.getFrameCount() - 2);
            j2 = jointProvider.getFirstJoint(jointProvider.getFrameCount() - 1);
        }

        return (float) (VectMath.distance(j1, j2) / jointProvider.getFrameDuration());
            
    }

    private float getExtendedOneFrameVelocity(int frameNo) {
        if (frameNo < 0) {
            return getExtendedOneFrameVelocity(-frameNo);
        }

        if (frameNo < jointProvider.getFrameCount()) {
            return getOneFrameVelocity(frameNo);
        }

        return getExtendedOneFrameVelocity(((frameNo - (jointProvider.getFrameCount() - 1)) % (2 * jointProvider.getFrameCount() - 2) - (jointProvider.getFrameCount() - 1)));
    }

    private double calculateNumerator(int k, int kMaxIncl) {
        double beta = 2.5;
        double param1 = (beta * 2 * k * (2 * kMaxIncl + 1));
        param1 = param1 * param1;
        return Math.pow(Math.E, -1 / 2 * param1);
    }

    private double calculateGauss(int k, int kMaxIncl) {
        double numerator = calculateNumerator(k, kMaxIncl);
        double denominator = 0;
        for (int i = -kMaxIncl; i <= kMaxIncl; i++) {
            denominator += calculateNumerator(i, kMaxIncl);
        }

        return numerator / denominator;
    }

    private float calculateNoiseReductedVelocity(int frameNo) {
        double vNoiseLess = 0;
        for (int i = -K_MAX; i <= K_MAX; i++) {
            vNoiseLess += calculateGauss(i, K_MAX) * getExtendedOneFrameVelocity(frameNo - i);
        }
        return (float) vNoiseLess;
    }

    public JointProvider getJointProvider() {
        return jointProvider;
    }

    public void setJointProvider(JointProvider jointProvider) {
        this.jointProvider = jointProvider;
    }

   
}
