/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mcda.commons.extraction.joint;

/**
 *
 * @author xvalcik
 */
public interface JointProvider {
    double[] getFirstJoint(int frameNo);
    int getFrameCount();
    float getFrameDuration();
}
