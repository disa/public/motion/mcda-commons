package mcda.commons.gait.domain;

import java.util.Arrays;

/**
 *
 * @author Jakub Valcik, xvalcik@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class CyclicBoneBuffer {

    float[] data;
    int end = 0;
    boolean full = false;

    private static final float PERCENTAGE_THRESHOLD = 0.9f;
    private static final float ALLOWED_DEVIATION = 0.01f;

    public CyclicBoneBuffer(int length) {
        data = new float[length];
    }

    public boolean add(float value) {
        if (!full || !isUnderThreshold()) {
            data[end] = value;
            if ((end + 1) % data.length == 0) {
                full = true;
            }
            end = (end + 1) % data.length;
            return true;
        }
        return false;
    }

    public float avg() {
        float sum = 0;
        for (float v : data) {
            sum += v;
        }
        return sum / data.length;
    }

    public float median() {
        float[] tmp = new float[data.length];
        System.arraycopy(data, 0, tmp, 0, data.length);
        Arrays.sort(tmp);
        return tmp.length % 2 == 0 ? (tmp[tmp.length / 2 - 1] + tmp[tmp.length / 2]) / 2 : tmp[tmp.length / 2];
    }

    public float stddev() {
        float sum = 0;
        float sumSq = 0;
        for (float v : data) {
            sum += v;
            sumSq += v * v;
        }
        return (float) Math.sqrt(((1.0 / (data.length - 1.0)) * (sumSq - data.length * (sum / data.length) * (sum / data.length))));
    }

    public float[] getData() {
        return data;
    }

    public boolean isUnderThreshold() {
        return calcPercentageUnderThreshold() > PERCENTAGE_THRESHOLD;
    }

    public boolean isFull() {
        return full;
    }

    public float calcPercentageUnderThreshold() {
        if (!full) {
            return Float.NaN;
        }
        float avg = avg();
        int counter = 0;
        for (float v : data) {
            if (Math.abs(v - avg) <= ALLOWED_DEVIATION) {
                ++counter;
            }
        }
        return counter / (float) data.length;
    }

}
