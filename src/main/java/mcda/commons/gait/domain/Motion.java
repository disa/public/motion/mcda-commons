/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mcda.commons.constants.LandmarkConstant;
import mcda.commons.domain.SpanRange;
import mcda.commons.extraction.joint.JointProvider;
import mcda.commons.extraction.joint.SmoothedJointSpeed;
import mcda.commons.math.Fft;
import mcda.commons.math.VectMath;

/**
 *
 * @author xvalcik
 */
public class Motion {

    private final float[][][] poses;
    private final Map<String, List<SpanRange>> spans = new HashMap<>();
    private final Map<String, int[]> markers = new HashMap<>();
    private final Map<String, float[]> features = new HashMap<>();
    private final Map<String, String> properties = new HashMap<>();
    private float[][] speedFft = null;
    private int lastHarmonics = -1;
    private int lastFps = -1;

    private static final int[] BONE_LENGTH_LANDMARK_COUPLES = new int[]{
        LandmarkConstant.LANDMARK_COUPLE_RFEMUR_RTIBIA_ID,
        LandmarkConstant.LANDMARK_COUPLE_LFEMUR_LTIBIA_ID,
        LandmarkConstant.LANDMARK_COUPLE_RHIPJOINT_RFEMUR_ID,
        LandmarkConstant.LANDMARK_COUPLE_LHIPJOINT_LFEMUR_ID,
        LandmarkConstant.LANDMARK_COUPLE_RHUMERUS_RRADIUS_ID,
        LandmarkConstant.LANDMARK_COUPLE_LHUMERUS_LRADIUS_ID,
        LandmarkConstant.LANDMARK_COUPLE_RCLAVICLE_RHUMERUS_ID,
        LandmarkConstant.LANDMARK_COUPLE_LCLAVICLE_LHUMERUS_ID,
        LandmarkConstant.LANDMARK_COUPLE_LHIPJOINT_RHIPJOINT_ID,
        LandmarkConstant.LANDMARK_COUPLE_LCLAVICLE_RCLAVICLE_ID,
        LandmarkConstant.LANDMARK_COUPLE_ROOT_LOWERBACK_ID,
        LandmarkConstant.LANDMARK_COUPLE_LOWERBACK_THORAX_ID,};

    private static final int BUFFER_LENGTH = 90;

    public Motion(float[][][] poses) {
        this.poses = poses;
    }

    public float[][][] getPoses() {
        return poses;
    }

    public Map<String, List<SpanRange>> getSpans() {
        return spans;
    }

    public Map<String, float[]> getFeatures() {
        return features;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public float[] getDtdsByLandmarkCoupleId(int landMarkCoupleId) {
        float[] result = new float[poses.length];
        for (int i = 0; i < result.length; i++) {
            float[] pos1 = poses[i][LandmarkConstant.getLandmarkPos(LandmarkConstant.LANDMARK_COUPLE_MAP.get(landMarkCoupleId)[0])];
            float[] pos2 = poses[i][LandmarkConstant.getLandmarkPos(LandmarkConstant.LANDMARK_COUPLE_MAP.get(landMarkCoupleId)[1])];
            result[i] = VectMath.distance(pos1, pos2);
        }
        return result;
    }

    public Map<String, int[]> getMarkers() {
        return markers;
    }

    public float[] getBoneLengths() {
        float[] result = new float[BONE_LENGTH_LANDMARK_COUPLES.length];
        for (int i = 0; i < result.length; i++) {
            float[] dtds = getDtdsByLandmarkCoupleId(BONE_LENGTH_LANDMARK_COUPLES[i]);
            CyclicBoneBuffer buf = new CyclicBoneBuffer(Math.min(dtds.length, BUFFER_LENGTH));
            float minPercentageUnderThreshold = Float.MAX_VALUE;
            float bestValue = Float.NaN;
            for (float boneLength : dtds) {
                buf.add(boneLength);
                if (buf.isFull() && minPercentageUnderThreshold > buf.calcPercentageUnderThreshold()) {
                    bestValue = buf.median();
                    minPercentageUnderThreshold = buf.calcPercentageUnderThreshold();
                }
                if (buf.isFull() && buf.isUnderThreshold()) {
                    bestValue = buf.median();
                    break;
                }
            }
            result[i] = bestValue;
        }
        return result;
    }

    public float[][] getSpeedFft(final int harmonics, final int fps) {
        if (speedFft == null || harmonics != lastHarmonics || fps != lastFps) {
            speedFft = extractSpeedFft(harmonics, fps);
            lastFps = fps;
            lastHarmonics = harmonics;
        }
        return speedFft;
    }

    private float[][] extractSpeedFft(final int harmonics, final int fps) {
        float[][] result = new float[LandmarkConstant.LANDMARK_COUNT][];

        for (int i = 0; i < result.length; i++) {
            if (LandmarkConstant.KINECT2_MOCAP_MAP.values().contains(i + 1/*joints are index from 1*/)) {
                double[] re = calculateSpeed(i, fps);
                double[] im = new double[poses.length];
                float[] fftJointSpeed = new float[harmonics];

                Fft.transform(re, im);
                for (int j = 0; j < re.length && j < harmonics; j++) {
                    re[j] = re[j] / re.length;
                    im[j] = im[j] / im.length;
                    double abs = Math.sqrt(re[j] * re[j] + im[j] * im[j]);
                    fftJointSpeed[j] = (float) abs;
                }
                result[i] = fftJointSpeed;
            }
        }

        return result;
    }

    public float extractJointAngle(int joint1, int joint2, int joint3, int frame) {
        float[] j1 = poses[frame][LandmarkConstant.getLandmarkPos(joint1)];
        float[] j2 = poses[frame][LandmarkConstant.getLandmarkPos(joint2)];
        float[] j3 = poses[frame][LandmarkConstant.getLandmarkPos(joint3)];

        double[] v1 = new double[3];
        double[] v2 = new double[3];
        for (int i = 0; i < v1.length; i++) {
            v1[i] = j2[i] - j1[i];
            v2[i] = j2[i] - j3[i];
        }
        return (float) ((Math.acos(VectMath.dotProduct3D(v1, v2) / (VectMath.size3D(v1) * VectMath.size3D(v2))) * 180 / Math.PI));
    }

    public double[] calculateSpeed(final int jointIndex, final int fps) {
        SmoothedJointSpeed speedExtractor = new SmoothedJointSpeed();
        speedExtractor.setJointProvider(new JointProvider() {

            @Override
            public double[] getFirstJoint(int frameNo) {
                float[] fJoint = poses[frameNo][jointIndex];
                double[] result = new double[fJoint.length];
                for (int i = 0; i < fJoint.length; i++) {
                    result[i] = fJoint[i];
                }
                return result;
            }

            @Override
            public int getFrameCount() {
                return poses.length;
            }

            @Override
            public float getFrameDuration() {
                return 1.0f / fps;
            }
        });

        double[] result = new double[poses.length];
        for (int i = 0; i < poses.length; i++) {
            result[i] = speedExtractor.computeDispatchingValue(i + 1);
        }
        return result;
    }
}
