/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author Kuba
 */
public class FeetMinMaxAltFemurTibiaValuesInMarkersAltSpan extends SpanWorker {

    @Override
    public List<SpanRange> findSpans(Motion m) {
        List<SpanRange> nonQuasiConstantSpan = m.getSpans().get(ExceptQuasiConstantSpans.class.getSimpleName());
        List<SpanRange> lFootRFootMinMaxAlternatingSpan = m.getSpans().get(LFootRFootMinMaxAlternatingSpan.class.getSimpleName());
        List<SpanRange> lFemurRTibiaValuesInMarkersAlternatingSpan = m.getSpans().get(LFemurRTibiaValuesInMarkersAlternatingSpan.class.getSimpleName());

        Set<SpanRange> intersection = SpanRange.spanIntersect(nonQuasiConstantSpan, lFootRFootMinMaxAlternatingSpan);
        intersection = SpanRange.spanIntersect(intersection, lFemurRTibiaValuesInMarkersAlternatingSpan);
        return new ArrayList<>(intersection);
    }

}
