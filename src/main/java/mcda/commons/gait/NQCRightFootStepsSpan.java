/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.ArrayList;
import java.util.List;
import mcda.commons.constants.LandmarkConstant;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author kuba
 */
public class NQCRightFootStepsSpan extends SpanWorker {

    @Override
    public List<SpanRange> findSpans(Motion m) {
        List<SpanRange> footSteps = m.getSpans().get(NQCFootStepsSpan.class.getSimpleName());

        List<SpanRange> rightFootSteps = new ArrayList<>();
        float[] lfemurRtibia = m.getDtdsByLandmarkCoupleId(LandmarkConstant.LANDMARK_COUPLE_LFEMUR_RTIBIA_ID);

        for (SpanRange in : footSteps) {
            if (lfemurRtibia[in.getFrameFrom()] > lfemurRtibia[in.getFrameTo()]) {
                rightFootSteps.add(in);
            }
        }

        return rightFootSteps;
    }
}
