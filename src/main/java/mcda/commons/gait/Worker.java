/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import mcda.commons.gait.domain.Motion;

/**
 *
 * @author xvalcik
 */
public abstract class Worker {

    public final String getIdentificationName() {
        return this.getClass().getSimpleName();
    }

    public abstract void process(Motion m);
}
