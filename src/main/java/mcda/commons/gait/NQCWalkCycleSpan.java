/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.logging.Level;
import java.util.logging.Logger;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author xvalcik
 */
public class NQCWalkCycleSpan {

    private static final Logger logger = Logger.getLogger(NQCWalkCycleSpan.class.getName());

    private static final Worker[] workers = {
        new ExceptQuasiConstantSpans(true),
        new LFootRFootMinimaOnNQCSpanMarker(true),
        new LFootRFootMaximaOnNQCSpanMarker(true),
        new LFootRFootMinMaxAlternatingSpan(),
        new LFemurRTibiaValuesInMarkersAlternatingSpan(),
        new FeetMinMaxAltFemurTibiaValuesInMarkersAltSpan(),
        new NQCFootStepsSpan(),
        new NQCLeftFootStepsSpan(),
        new NQCRightFootStepsSpan(),
        new NQCWalkCycleNormalizationSpan(),};

    public static void process(Motion m) {
        for (int i = 0; i < workers.length; i++) {
            Worker worker = workers[i];
            logger.log(Level.INFO, "Starting {0} worker ({1}/{2}).", new Object[]{worker.getIdentificationName(), i + 1, workers.length});
            worker.process(m);
            logger.log(Level.INFO, "{0} worker finished.", worker.getIdentificationName());
        }
    }

}
