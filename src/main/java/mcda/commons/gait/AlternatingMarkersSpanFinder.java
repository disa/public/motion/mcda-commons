/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import mcda.commons.domain.SpanRange;
import java.util.*;

/**
 *
 * @author Kuba
 */
public class AlternatingMarkersSpanFinder extends SpanFinder {

    private static final int SIG_1 = 1;
    private static final int SIG_2 = 2;

    public List<SpanRange> find(int[] marker1, int[] marker2, int minSkipCount, boolean sig1SkipEnforce, SpanRange spanRange) {
        return find(markerSubsetWithinRange(marker1, spanRange), markerSubsetWithinRange(marker2, spanRange), minSkipCount, sig1SkipEnforce);
    }

    public List<SpanRange> find(int[] marker1, int[] marker2, int minSkipCount, boolean sig1SkipEnforce) {
        List<SpanRange> result = new ArrayList<>();
        List<MarkerIdentificator> markers = new ArrayList<>();
        for (int md : marker1) {
            markers.add(new MarkerIdentificator(md, SIG_1));
        }
        for (int md : marker2) {
            markers.add(new MarkerIdentificator(md, SIG_2));
        }

        Collections.sort(markers);

        int begin = -1;

        for (int i = 0; i < markers.size() - 1; i++) {
            if (sig1SkipEnforce && begin == -1 && SIG_1 != markers.get(i).getIdentificator()) {
                continue;
            }
            if (markers.get(i).getIdentificator() != markers.get(i + 1).getIdentificator()) {
                //in interval
                if (begin == -1) {
                    begin = i;
                }
            } else {
                //not in interval
                if (begin != -1) {
                    int end = i;
                    if (sig1SkipEnforce && SIG_1 != markers.get(end).getIdentificator()) {
                        --end;
                    }
                    if (begin < end + 1 - minSkipCount) {
                        result.add(new SpanRange(markers.get(begin).getValue(), markers.get(end).getValue()));
                    }
                    begin = -1;
                }
            }

        }

        if (begin != -1) {
            int end = markers.size() - 1;
            if (sig1SkipEnforce && SIG_1 != markers.get(end).getIdentificator()) {
                --end;
            }
            if (begin < end + 1 - minSkipCount) {
                result.add(new SpanRange(markers.get(begin).getValue(), markers.get(end).getValue()));
            }
        }

        return result;
    }

    private class MarkerIdentificator implements Comparable<MarkerIdentificator> {

        private final int value;
        private final int identificator;

        public MarkerIdentificator(int value, int identificator) {
            this.value = value;
            this.identificator = identificator;
        }

        public int getValue() {
            return value;
        }

        public int getIdentificator() {
            return identificator;
        }

        @Override
        public int compareTo(MarkerIdentificator o) {
            return Integer.compare(value, o.value);
        }
    }
}
