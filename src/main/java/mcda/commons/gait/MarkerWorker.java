package mcda.commons.gait;

import java.util.ArrayList;
import java.util.List;
import mcda.commons.analysis.ExtremeFinder;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 * Common Worker for MarkerWorkers. Offers common marker processing. Creates
 * Marker, MarkerDescription and converts int[] to MarkerData entity and add
 * Marker to sequence.
 *
 * @author kuba
 */
public abstract class MarkerWorker extends Worker {

    @Override
    public final void process(Motion m) {
        m.getMarkers().put(getIdentificationName(), findMarkers(m));
    }

    public abstract int[] findMarkers(Motion m);

    public int[] findExtremes(ExtremeFinder extremeFinder, float[] dtds) {
        int[] markers = toIntArray(extremeFinder.findExtremes(dtds));
        return markers;
    }

    public int[] findExtremesOnSpans(ExtremeFinder extremeFinder, float[] dtds, List<SpanRange> spans) {
        List<Integer> result = new ArrayList<>();
        for (SpanRange sData : spans) {
            float[] tmpData = new float[sData.getFrameTo() - sData.getFrameFrom() + 1];
            System.arraycopy(dtds, sData.getFrameFrom(), tmpData, 0, sData.getFrameTo() - sData.getFrameFrom() + 1);

            List<Integer> partialResult = extremeFinder.findExtremes(tmpData);
            for (Integer res : partialResult) {
                result.add(res + sData.getFrameFrom());
            }
        }

        return toIntArray(result);
    }

    private static int[] toIntArray(List<Integer> in) {
        int[] result = new int[in.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = in.get(i);
        }
        return result;
    }
}
