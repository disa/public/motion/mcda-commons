/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.ArrayList;
import java.util.List;
import mcda.commons.constants.KinectMeasureConstants;
import mcda.commons.constants.LandmarkConstant;
import mcda.commons.constants.MocapMeasureConstants;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author Kuba
 */
public class ExceptQuasiConstantSpans extends SpanWorker {

    private final boolean kinect;

    public ExceptQuasiConstantSpans() {
        this.kinect = false;
    }

    public ExceptQuasiConstantSpans(boolean kinect) {
        this.kinect = kinect;
    }

    private int chooseMinLengthOfNonQuasiConstantInterval() {
        return kinect ? KinectMeasureConstants.MIN_LENGTH_OF_NON_QUASI_CONSTANT_INTERVAL : MocapMeasureConstants.MIN_LENGTH_OF_NON_QUASI_CONSTANT_INTERVAL;
    }

    @Override
    public List<SpanRange> findSpans(Motion m) {
        QuasiConstantFinder qcf = new QuasiConstantFinder(kinect);
        float[] lFootRFoot = m.getDtdsByLandmarkCoupleId(LandmarkConstant.LANDMARK_COUPLE_LFOOT_RFOOT_ID);
        List<SpanRange> spans = qcf.find(lFootRFoot);
        spans = SpanRange.complement(spans, 0, lFootRFoot.length - 1);
        List<SpanRange> result = new ArrayList<>();
        for (SpanRange sr : spans) {
            if (sr.getFrameTo() - sr.getFrameFrom() > chooseMinLengthOfNonQuasiConstantInterval()) {
                result.add(sr);
            }
        }

        return result;
    }
}
