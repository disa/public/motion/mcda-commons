/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author kuba
 */
public class NQCFootStepsSpan extends SpanWorker {

    @Override
    public List<SpanRange> findSpans(Motion m) {
        List<SpanRange> posibleWalkSpan = m.getSpans().get(FeetMinMaxAltFemurTibiaValuesInMarkersAltSpan.class.getSimpleName());
        int[] minMarkers = m.getMarkers().get(LFootRFootMinimaOnNQCSpanMarker.class.getSimpleName());

        Arrays.sort(minMarkers);

        Set<SpanRange> posibleFootSteps = new HashSet<>();
        for (int i = 1; i < minMarkers.length; i++) {
            posibleFootSteps.add(new SpanRange(minMarkers[i - 1], minMarkers[i]));
        }

        Set<SpanRange> intersection = SpanRange.spanIntersect(posibleWalkSpan, posibleFootSteps, 1);
        return new ArrayList<>(intersection);
    }
}
