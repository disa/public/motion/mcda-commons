/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import mcda.commons.domain.SpanRange;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Kuba
 */
public class AlternatingValuesInMarkersSpanFinder extends SpanFinder {

    public List<SpanRange> find(float[] dtds, int[] markers, SpanRange spanRange) {
        return find(dtds, markerSubsetWithinRange(markers, spanRange));
    }

    public List<SpanRange> find(float[] dtds, int[] markers) {
        List<SpanRange> result = new ArrayList<>();

        int[] tmpMarkers = new int[markers.length];
        System.arraycopy(markers, 0, tmpMarkers, 0, markers.length);

        Arrays.sort(tmpMarkers);

        int begin = -1;
        for (int i = 1; i < tmpMarkers.length - 1; i++) {

            if ((dtds[tmpMarkers[i - 1]] > dtds[tmpMarkers[i]]
                    && dtds[tmpMarkers[i]] < dtds[tmpMarkers[i + 1]])
                    || (dtds[tmpMarkers[i - 1]] < dtds[tmpMarkers[i]]
                    && dtds[tmpMarkers[i]] > dtds[tmpMarkers[i + 1]])) {
                //in interval
                if (begin == -1) {
                    begin = i - 1;
                }
            } else {
                //not in interval
                if (begin != -1) {
                    result.add(new SpanRange(tmpMarkers[begin], tmpMarkers[i]));
                    begin = -1;
                }
            }
        }

        if (begin != -1) {
            result.add(new SpanRange(tmpMarkers[begin], tmpMarkers[tmpMarkers.length - 1]));
        }

        return result;
    }
}
