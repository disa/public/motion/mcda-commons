/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.ArrayList;
import java.util.List;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author Kuba
 */
public class LFootRFootMinMaxAlternatingSpan extends SpanWorker {

    private static final int MIN_SKIP_COUNT = 4;

    @Override
    public List<SpanRange> findSpans(Motion m) {
        List<SpanRange> nonQuasiConstantSpan = m.getSpans().get(ExceptQuasiConstantSpans.class.getSimpleName());
        int[] minMarkers = m.getMarkers().get(LFootRFootMinimaOnNQCSpanMarker.class.getSimpleName());
        int[] maxMarkers = m.getMarkers().get(LFootRFootMaximaOnNQCSpanMarker.class.getSimpleName());

        AlternatingMarkersSpanFinder finder = new AlternatingMarkersSpanFinder();

        List<SpanRange> result = new ArrayList<>();
        for (SpanRange sData : nonQuasiConstantSpan) {
            result.addAll(finder.find(minMarkers, maxMarkers, MIN_SKIP_COUNT, true, new SpanRange(sData.getFrameFrom(), sData.getFrameTo())));
        }
        return result;
    }

}
