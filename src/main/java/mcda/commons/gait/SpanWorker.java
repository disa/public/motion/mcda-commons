package mcda.commons.gait;

import java.util.List;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 * Common Worker for SpanWorkers. Offers common span processing. Creates Span,
 * SpanDescription and converts SpanRange to SpanData and add Span to sequence.
 *
 * @author kuba
 */
public abstract class SpanWorker extends Worker {

    @Override
    public final void process(Motion m) {
        m.getSpans().put(getIdentificationName(), findSpans(m));
    }

    public abstract List<SpanRange> findSpans(Motion m);
}
