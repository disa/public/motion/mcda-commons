/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.ArrayList;
import java.util.List;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author kuba
 */
public class NQCWalkCycleNormalizationSpan extends SpanWorker {

    @Override
    public List<SpanRange> findSpans(Motion m) {
        List<SpanRange> leftFootSteps = m.getSpans().get(NQCLeftFootStepsSpan.class.getSimpleName());
        List<SpanRange> rightFootSteps = m.getSpans().get(NQCRightFootStepsSpan.class.getSimpleName());

        List<SpanRange> walkCycles = new ArrayList<>();
        for (SpanRange left : leftFootSteps) {
            for (SpanRange right : rightFootSteps) {
                if (left.getFrameTo() == right.getFrameFrom()) {
                    walkCycles.add(new SpanRange(left.getFrameFrom(), right.getFrameTo()));
                }
            }
        }

        return walkCycles;
    }
}
