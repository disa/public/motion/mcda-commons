/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.ArrayList;
import java.util.List;
import mcda.commons.constants.LandmarkConstant;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author Kuba
 */
public class LFemurRTibiaValuesInMarkersAlternatingSpan extends SpanWorker {

    @Override
    public List<SpanRange> findSpans(Motion m) {

        List<SpanRange> nonQuasiConstantSpan = m.getSpans().get(ExceptQuasiConstantSpans.class.getSimpleName());
        int[] minMarkers = m.getMarkers().get(LFootRFootMinimaOnNQCSpanMarker.class.getSimpleName());
        float[] lFemurRTibia = m.getDtdsByLandmarkCoupleId(LandmarkConstant.LANDMARK_COUPLE_LFEMUR_RTIBIA_ID);

        AlternatingValuesInMarkersSpanFinder finder = new AlternatingValuesInMarkersSpanFinder();

        List<SpanRange> result = new ArrayList<>();
        for (SpanRange sData : nonQuasiConstantSpan) {
            result.addAll(finder.find(lFemurRTibia, minMarkers, new SpanRange(sData.getFrameFrom(), sData.getFrameTo())));
        }
        return result;
    }

}
