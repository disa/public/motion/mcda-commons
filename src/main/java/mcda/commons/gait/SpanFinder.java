/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.SortedSet;
import java.util.TreeSet;
import mcda.commons.domain.SpanRange;

/**
 *
 * @author xvalcik
 */
public class SpanFinder {

    public static int[] markerSubsetWithinRange(int[] markers, SpanRange spanRange) {
        SortedSet<Integer> sortedMarker = new TreeSet();
        for (int m : markers) {
            sortedMarker.add(m);
        }
        SortedSet<Integer> subSet = sortedMarker.subSet(spanRange.getFrameFrom(), spanRange.getFrameTo() + 1);
        int[] result = new int[subSet.size()];
        int idx = 0;
        for (Integer m : subSet) {
            result[idx++] = m;
        }
        return result;
    }
}
