/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.gait;

import java.util.List;
import mcda.commons.analysis.MaximaFinder;
import mcda.commons.constants.LandmarkConstant;
import mcda.commons.domain.SpanRange;
import mcda.commons.gait.domain.Motion;

/**
 *
 * @author Kuba
 */
public class LFootRFootMaximaOnNQCSpanMarker extends MarkerWorker {

    private final boolean kinect;

    public LFootRFootMaximaOnNQCSpanMarker() {
        this.kinect = false;
    }

    public LFootRFootMaximaOnNQCSpanMarker(boolean kinect) {
        this.kinect = kinect;
    }

    @Override
    public int[] findMarkers(Motion m) {
        List<SpanRange> nonQuasiConstantSpan = m.getSpans().get(ExceptQuasiConstantSpans.class.getSimpleName());
        float[] lFootRFoot = m.getDtdsByLandmarkCoupleId(LandmarkConstant.LANDMARK_COUPLE_LFOOT_RFOOT_ID);

        return findExtremesOnSpans(new MaximaFinder(kinect), lFootRFoot, nonQuasiConstantSpan);
    }
}
