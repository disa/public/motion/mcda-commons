package mcda.commons.constants;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author kuba
 */
public class LandmarkConstant {

    //************ Landmark IDs ************//
    public static final int LANDMARK_ROOT_ID = 1;
    public static final int LANDMARK_LHIPJOINT_ID = 2;
    public static final int LANDMARK_LFEMUR_ID = 3;
    public static final int LANDMARK_LTIBIA_ID = 4;
    public static final int LANDMARK_LFOOT_ID = 5;
    public static final int LANDMARK_LTOES_ID = 6;
    public static final int LANDMARK_RHIPJOINT_ID = 7;
    public static final int LANDMARK_RFEMUR_ID = 8;
    public static final int LANDMARK_RTIBIA_ID = 9;
    public static final int LANDMARK_RFOOT_ID = 10;
    public static final int LANDMARK_RTOES_ID = 11;
    public static final int LANDMARK_LOWERBACK_ID = 12;
    public static final int LANDMARK_UPPERBACK_ID = 13;
    public static final int LANDMARK_THORAX_ID = 14;
    public static final int LANDMARK_LOWERNECK_ID = 15;
    public static final int LANDMARK_UPPERNECK_ID = 16;
    public static final int LANDMARK_HEAD_ID = 17;
    public static final int LANDMARK_LCLAVICLE_ID = 18;
    public static final int LANDMARK_LHUMERUS_ID = 19;
    public static final int LANDMARK_LRADIUS_ID = 20;
    public static final int LANDMARK_LWRIST_ID = 21;
    public static final int LANDMARK_LHAND_ID = 22;
    public static final int LANDMARK_LFINGERS_ID = 23;
    public static final int LANDMARK_LTHUMB_ID = 24;
    public static final int LANDMARK_RCLAVICLE_ID = 25;
    public static final int LANDMARK_RHUMERUS_ID = 26;
    public static final int LANDMARK_RRADIUS_ID = 27;
    public static final int LANDMARK_RWRIST_ID = 28;
    public static final int LANDMARK_RHAND_ID = 29;
    public static final int LANDMARK_RFINGERS_ID = 30;
    public static final int LANDMARK_RTHUMB_ID = 31;

    public static final int LANDMARK_COUNT = 31;
    //************ Landmark couple IDs ************//
    public static final int LANDMARK_COUPLE_LFOOT_RFOOT_ID = 1;
    // upper quadrangle
    public static final int LANDMARK_COUPLE_LFEMUR_RFEMUR_ID = 7;
    public static final int LANDMARK_COUPLE_RHIPJOINT_RFEMUR_ID = 12;
    public static final int LANDMARK_COUPLE_LFEMUR_RHIPJOINT_ID = 10;
    public static final int LANDMARK_COUPLE_LHIPJOINT_LFEMUR_ID = 8;
    public static final int LANDMARK_COUPLE_LHIPJOINT_RFEMUR_ID = 11;
    // lower quadrangle
    public static final int LANDMARK_COUPLE_LTIBIA_RTIBIA_ID = 3;
    public static final int LANDMARK_COUPLE_RFEMUR_RTIBIA_ID = 6;
    public static final int LANDMARK_COUPLE_LTIBIA_RFEMUR_ID = 4;
    public static final int LANDMARK_COUPLE_LFEMUR_LTIBIA_ID = 5;
    public static final int LANDMARK_COUPLE_LFEMUR_RTIBIA_ID = 2;
    public static final int LANDMARK_COUPLE_LHIPJOINT_LFOOT_ID = 14;
    public static final int LANDMARK_COUPLE_LHIPJOINT_RFOOT_ID = 15;
    public static final int LANDMARK_COUPLE_LFOOT_RHIPJOINT_ID = 16;
    public static final int LANDMARK_COUPLE_RHIPJOINT_RFOOT_ID = 9;
    public static final int LANDMARK_COUPLE_RCLAVICLE_RHAND_ID = 18;
    public static final int LANDMARK_COUPLE_LCLAVICLE_LHAND_ID = 17;
    public static final int LANDMARK_COUPLE_LHIPJOINT_RHIPJOINT_ID = 13;
    public static final int LANDMARK_COUPLE_LHIPJOINT_LTIBIA_ID = 19;
    public static final int LANDMARK_COUPLE_LHIPJOINT_RTIBIA_ID = 20;
    public static final int LANDMARK_COUPLE_LTIBIA_RHIPJOINT_ID = 21;
    public static final int LANDMARK_COUPLE_RHIPJOINT_RTIBIA_ID = 22;
    public static final int LANDMARK_COUPLE_LCLAVICLE_RHAND_ID = 23;
    public static final int LANDMARK_COUPLE_LHAND_RCLAVICLE_ID = 24;
    public static final int LANDMARK_COUPLE_LHUMERUS_RHAND_ID = 25;
    public static final int LANDMARK_COUPLE_LHAND_RHUMERUS_ID = 26;
    public static final int LANDMARK_COUPLE_LHAND_RHAND_ID = 27;
    public static final int LANDMARK_COUPLE_RFOOT_LHAND_ID = 28;
    public static final int LANDMARK_COUPLE_LFEMUR_RFOOT_ID = 29;
    public static final int LANDMARK_COUPLE_LTOES_RFEMUR_ID = 30;
    public static final int LANDMARK_COUPLE_LFOOT_LHAND_ID = 31;
    public static final int LANDMARK_COUPLE_RFOOT_RHAND_ID = 34;
    public static final int LANDMARK_COUPLE_LFOOT_RHAND_ID = 35;
    public static final int LANDMARK_COUPLE_LFOOT_RFEMUR_ID = 36;
    public static final int LANDMARK_COUPLE_RHIPJOINT_RHAND_ID = 37;
    public static final int LANDMARK_COUPLE_LHIPJOINT_LHAND_ID = 38;
    public static final int LANDMARK_COUPLE_HEAD_LCLAVICLE_ID = 39;
    public static final int LANDMARK_COUPLE_HEAD_RCLAVICLE_ID = 40;
    public static final int LANDMARK_COUPLE_LHUMERUS_RHUMERUS_ID = 41;

    public static final int LANDMARK_COUPLE_LCLAVICLE_LHUMERUS_ID = 56;
    public static final int LANDMARK_COUPLE_LHUMERUS_LRADIUS_ID = 57;
    public static final int LANDMARK_COUPLE_RCLAVICLE_RHUMERUS_ID = 58;
    public static final int LANDMARK_COUPLE_RHUMERUS_RRADIUS_ID = 59;

    public static final int LANDMARK_COUPLE_LCLAVICLE_RCLAVICLE_ID = 72;
    public static final int LANDMARK_COUPLE_ROOT_LOWERBACK_ID = 44;
    public static final int LANDMARK_COUPLE_LOWERBACK_THORAX_ID = 45;

    //************ Methods ************//
    /**
     * Returns the list (array) position of the given landmark.
     *
     * @param landmarkId landmark specified by its id
     * @return list (array) position of the given landmark
     */
    public static int getLandmarkPos(int landmarkId) {
        return landmarkId - 1;
    }

    public static final Map<Integer, Integer> KINECT2_MOCAP_MAP = new HashMap<>();
    public static final Map<Integer, int[]> LANDMARK_COUPLE_MAP = new HashMap<>();

    static {
        //root
        KINECT2_MOCAP_MAP.put(0, LANDMARK_ROOT_ID);

        //l leg
        KINECT2_MOCAP_MAP.put(12, LANDMARK_LHIPJOINT_ID);
        KINECT2_MOCAP_MAP.put(13, LANDMARK_LFEMUR_ID);
        KINECT2_MOCAP_MAP.put(14, LANDMARK_LTIBIA_ID);
        KINECT2_MOCAP_MAP.put(15, LANDMARK_LFOOT_ID);

        //r leg
        KINECT2_MOCAP_MAP.put(16, LANDMARK_RHIPJOINT_ID);
        KINECT2_MOCAP_MAP.put(17, LANDMARK_RFEMUR_ID);
        KINECT2_MOCAP_MAP.put(18, LANDMARK_RTIBIA_ID);
        KINECT2_MOCAP_MAP.put(19, LANDMARK_RFOOT_ID);

        //torso
        KINECT2_MOCAP_MAP.put(1, LANDMARK_LOWERBACK_ID); //SpineMid->"lowerback"
        KINECT2_MOCAP_MAP.put(20, LANDMARK_LOWERNECK_ID); //SpineShoulder->"lowerneck"
        KINECT2_MOCAP_MAP.put(2, LANDMARK_UPPERNECK_ID); //Neck->"upperneck"
        KINECT2_MOCAP_MAP.put(3, LANDMARK_HEAD_ID); //Head->"head"

        //l hand
        KINECT2_MOCAP_MAP.put(4, LANDMARK_LCLAVICLE_ID); //ShoulderLeft->"lclavicle"
        KINECT2_MOCAP_MAP.put(5, LANDMARK_LHUMERUS_ID); //ElbowLeft->"lhumerus"
        KINECT2_MOCAP_MAP.put(6, LANDMARK_LWRIST_ID); //WristLeft->"lradius"
        KINECT2_MOCAP_MAP.put(7, LANDMARK_LHAND_ID); //HandLeft->"lhand"
        KINECT2_MOCAP_MAP.put(21, LANDMARK_LFINGERS_ID); //HandTipLeft->"lfingers"
        KINECT2_MOCAP_MAP.put(22, LANDMARK_LTHUMB_ID); //ThumbLeft->"lthumb"

        //r hand
        KINECT2_MOCAP_MAP.put(8, LANDMARK_RCLAVICLE_ID); //ShoulderRight->"rclavicle"
        KINECT2_MOCAP_MAP.put(9, LANDMARK_RHUMERUS_ID); //ElbowRight->"rhumerus"
        KINECT2_MOCAP_MAP.put(10, LANDMARK_RWRIST_ID); //WristRight->"rradius"
        KINECT2_MOCAP_MAP.put(11, LANDMARK_RHAND_ID); //HandRight->"rhand"
        KINECT2_MOCAP_MAP.put(23, LANDMARK_RFINGERS_ID); //HandTipRight->"rfingers"
        KINECT2_MOCAP_MAP.put(24, LANDMARK_RTHUMB_ID); //ThumbRight->"rthumb"

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_ROOT_LOWERBACK_ID, new int[]{LANDMARK_ROOT_ID, LANDMARK_LOWERBACK_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHIPJOINT_LFEMUR_ID, new int[]{LANDMARK_LHIPJOINT_ID, LANDMARK_LFEMUR_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHIPJOINT_RHIPJOINT_ID, new int[]{LANDMARK_LHIPJOINT_ID, LANDMARK_RHIPJOINT_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHIPJOINT_RFEMUR_ID, new int[]{LANDMARK_LHIPJOINT_ID, LANDMARK_RFEMUR_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHIPJOINT_RTIBIA_ID, new int[]{LANDMARK_LHIPJOINT_ID, LANDMARK_RTIBIA_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHIPJOINT_RFOOT_ID, new int[]{LANDMARK_LHIPJOINT_ID, LANDMARK_RFOOT_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHIPJOINT_LHAND_ID, new int[]{LANDMARK_LHIPJOINT_ID, LANDMARK_LHAND_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LFEMUR_LTIBIA_ID, new int[]{LANDMARK_LFEMUR_ID, LANDMARK_LTIBIA_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LFEMUR_RTIBIA_ID, new int[]{LANDMARK_LFEMUR_ID, LANDMARK_RTIBIA_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LFOOT_RFOOT_ID, new int[]{LANDMARK_LFOOT_ID, LANDMARK_RFOOT_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LFOOT_RHAND_ID, new int[]{LANDMARK_LFOOT_ID, LANDMARK_RHAND_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RHIPJOINT_RFEMUR_ID, new int[]{LANDMARK_RHIPJOINT_ID, LANDMARK_RFEMUR_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RHIPJOINT_RTIBIA_ID, new int[]{LANDMARK_RHIPJOINT_ID, LANDMARK_RTIBIA_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RHIPJOINT_RFOOT_ID, new int[]{LANDMARK_RHIPJOINT_ID, LANDMARK_RFOOT_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RHIPJOINT_RHAND_ID, new int[]{LANDMARK_RHIPJOINT_ID, LANDMARK_RHAND_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RFEMUR_RTIBIA_ID, new int[]{LANDMARK_RFEMUR_ID, LANDMARK_RTIBIA_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RFOOT_LHAND_ID, new int[]{LANDMARK_RFOOT_ID, LANDMARK_LHAND_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RFOOT_RHAND_ID, new int[]{LANDMARK_RFOOT_ID, LANDMARK_RHAND_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LOWERBACK_THORAX_ID, new int[]{LANDMARK_LOWERBACK_ID, LANDMARK_THORAX_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LCLAVICLE_LHUMERUS_ID, new int[]{LANDMARK_LCLAVICLE_ID, LANDMARK_LHUMERUS_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LCLAVICLE_RCLAVICLE_ID, new int[]{LANDMARK_LCLAVICLE_ID, LANDMARK_RCLAVICLE_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LCLAVICLE_RHAND_ID, new int[]{LANDMARK_LCLAVICLE_ID, LANDMARK_RHAND_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHUMERUS_LRADIUS_ID, new int[]{LANDMARK_LHUMERUS_ID, LANDMARK_LRADIUS_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHUMERUS_RHUMERUS_ID, new int[]{LANDMARK_LHUMERUS_ID, LANDMARK_RHUMERUS_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHUMERUS_RHAND_ID, new int[]{LANDMARK_LHUMERUS_ID, LANDMARK_RHAND_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHAND_RHUMERUS_ID, new int[]{LANDMARK_LHAND_ID, LANDMARK_RHUMERUS_ID});
        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_LHAND_RHAND_ID, new int[]{LANDMARK_LHAND_ID, LANDMARK_RHAND_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RCLAVICLE_RHUMERUS_ID, new int[]{LANDMARK_RCLAVICLE_ID, LANDMARK_RHUMERUS_ID});

        LANDMARK_COUPLE_MAP.put(LANDMARK_COUPLE_RHUMERUS_RRADIUS_ID, new int[]{LANDMARK_RHUMERUS_ID, LANDMARK_RRADIUS_ID});

    }
}
