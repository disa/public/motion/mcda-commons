/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.constants;

/**
 *
 * @author Kuba
 */
public class MocapMeasureConstants {
    
    public static final int MINIMA_FINDER_ALFA = 40;
    public static final int MAXIMA_FINDER_ALFA = 40;
    
    public static final int QUASI_CONSTANT_DELTA = 10;
    public static final int QUASI_CONSTANT_ALFA = 80;
    
    public static final int MIN_LENGTH_OF_NON_QUASI_CONSTANT_INTERVAL = 40;
}
