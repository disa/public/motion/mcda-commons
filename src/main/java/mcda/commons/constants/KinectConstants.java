/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mcda.commons.constants;

import mcda.commons.constants.LandmarkConstant;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author xvalcik
 */
public class KinectConstants {
    public static final Map<Integer, String> KINECT_JOINTS_TO_STRING = new HashMap<>();
    
    static{
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_ROOT_ID,"root");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LOWERBACK_ID, "lowerback");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LOWERNECK_ID, "lowerneck");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_HEAD_ID, "head");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LCLAVICLE_ID, "lclavicle");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LHUMERUS_ID, "lhumerus");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LRADIUS_ID, "lradius");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LHAND_ID, "lhand");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_RCLAVICLE_ID, "rclavicle");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_RHUMERUS_ID, "rhumerus");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_RRADIUS_ID, "rradius");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_RHAND_ID, "rhand");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LHIPJOINT_ID, "lhipjoint");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LFEMUR_ID, "lfemur");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LTIBIA_ID, "ltibia");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_LFOOT_ID, "lfoot");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_RHIPJOINT_ID, "rhipjoint");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_RFEMUR_ID, "rfemur");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_RTIBIA_ID, "rtibia");
        KINECT_JOINTS_TO_STRING.put(LandmarkConstant.LANDMARK_RFOOT_ID, "rfoot");
    }
}
