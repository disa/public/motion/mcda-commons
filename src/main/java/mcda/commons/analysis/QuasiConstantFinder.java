/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.analysis;

import mcda.commons.domain.SpanRange;
import mcda.commons.constants.MocapMeasureConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import mcda.commons.constants.KinectMeasureConstants;

/**
 *
 * @author Kuba
 */
public class QuasiConstantFinder {

    private final boolean kinect;

    public QuasiConstantFinder() {
        this.kinect = false;
    }

    public QuasiConstantFinder(boolean kinect) {
        this.kinect = kinect;
    }

    private double chooseQuasiConstantDelta() {
        return kinect ? KinectMeasureConstants.QUASI_CONSTANT_DELTA : MocapMeasureConstants.QUASI_CONSTANT_DELTA;
    }

    private int chooseQuasiConstantAlfa() {
        return kinect ? KinectMeasureConstants.QUASI_CONSTANT_ALFA : MocapMeasureConstants.QUASI_CONSTANT_ALFA;
    }

    public List<SpanRange> find(float[] sig) {

        List<SpanRange> result = new ArrayList<>();

        double min;
        double max;
        final double delta = chooseQuasiConstantDelta();//chooseDelta(sig);
        final int minFrames = chooseQuasiConstantAlfa();
        SpanRange lastSpanData = null;
        int i = 0;
        while (i < sig.length - minFrames) {
            min = sig[i];
            max = sig[i];
            int j = i + 1;
            while (j < sig.length && max - min < delta) {
                if (sig[j] > max) {
                    max = sig[j];
                }
                if (sig[j] < min) {
                    min = sig[j];
                }
                ++j;
            }
            if (j - i < minFrames) {
                ++i;
            } else {
                if (lastSpanData == null) {
                    lastSpanData = new SpanRange(i, j);
                } else {
                    if (lastSpanData.getFrameFrom() >= i) {
                        int from = lastSpanData.getFrameFrom();
                        lastSpanData = new SpanRange(from, j);
                    } else {
                        result.add(lastSpanData);
                        lastSpanData = new SpanRange(i, j);
                    }
                }
                ++i;
            }

        }
        if (lastSpanData != null) {
            result.add(lastSpanData);
        }
        return result;
    }

    private double chooseDelta(double[] data) {
        //double result = data[findGlobalMinimumIndex(data)] + (data[findGlobalMaximumIndex(data)] - data[findGlobalMinimumIndex(data)]) * 2 / 3;
        double[] tmp = new double[data.length];
        System.arraycopy(data, 0, tmp, 0, data.length);
        Arrays.sort(tmp);
        double result = tmp[(int) (tmp.length * 1.0 / 3)] - tmp[0];
        return result;
    }
}
