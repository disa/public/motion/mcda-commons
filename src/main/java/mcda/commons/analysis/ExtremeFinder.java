/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcda.commons.analysis;

import java.util.List;

/**
 *
 * @author kuba
 */
public interface ExtremeFinder {
    List<Integer> findExtremes(float[] data);
}
