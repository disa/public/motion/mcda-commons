/**
 *
 */
package mcda.commons.analysis;

import mcda.commons.constants.MocapMeasureConstants;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import mcda.commons.constants.KinectMeasureConstants;

/**
 * @author Kuba
 *
 */
public class MinimaFinder implements ExtremeFinder{
    
    private final boolean kinect;

    public MinimaFinder() {
        this.kinect = false;
    }

    public MinimaFinder(boolean kinect) {
        this.kinect = kinect;
    }
    
    

    protected double chooseBeta(float[] data) {
        // double result = data[findGlobalMinimumIndex(data)] + (data[findGlobalMaximumIndex(data)] - data[findGlobalMinimumIndex(data)]) * 2 / 3;
        float[] tmp = new float[data.length];
        System.arraycopy(data, 0, tmp, 0, data.length);
        Arrays.sort(tmp);
        double result = tmp[(int) (tmp.length * 1.0 / 3)];
        return result;
    }

    protected int chooseAlpha(float[] data) {
        return kinect?KinectMeasureConstants.MINIMA_FINDER_ALFA:MocapMeasureConstants.MINIMA_FINDER_ALFA;
    }

    protected List<Integer> findInterestingLocalMinima(float[] data, int alpha, double beta) {
        List<Integer> indices = new LinkedList<>();
        int alphaLimit;
        int minIndex;
        double minValue;
        int i = 0;
        while (i < data.length) {
            minIndex = -1;
            minValue = Double.MAX_VALUE;
            while (i < data.length && data[i] > beta) {
                ++i;
            }
            alphaLimit = i + alpha;
            while (i < alphaLimit && i < data.length) {
                if (data[i] < minValue) {
                    minIndex = i;
                    minValue = data[i];
                    alphaLimit = minIndex + alpha;
                }
                ++i;
            }
            if (minIndex > 0 && minIndex < data.length - 1) {
                indices.add(minIndex);
            }
        }

        return indices;
    }

    @Override
    public List<Integer> findExtremes(float[] data) {
        double beta = chooseBeta(data);
        int alpha = chooseAlpha(data);
        List<Integer> minima = findInterestingLocalMinima(data, alpha, beta);
        return minima;
    }
}
