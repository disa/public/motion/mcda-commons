/**
 *
 */
package mcda.commons.analysis;

import mcda.commons.constants.MocapMeasureConstants;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import mcda.commons.constants.KinectMeasureConstants;

/**
 * @author Kuba
 *
 */
public class MaximaFinder implements ExtremeFinder{

    private final boolean kinect;

    public MaximaFinder() {
        this.kinect = false;
    }

    public MaximaFinder(boolean kinect) {
        this.kinect = kinect;
    }
    
    
    
    private float chooseBeta(float[] data) {
        //double result = data[findGlobalMinimumIndex(data)] + (data[findGlobalMaximumIndex(data)] - data[findGlobalMinimumIndex(data)]) * 2 / 3;
        float[] tmp = new float[data.length];
        System.arraycopy(data, 0, tmp, 0, data.length);
        Arrays.sort(tmp);
        float result = tmp[tmp.length * 2 / 3];
        return result;
    }

    private int chooseAlpha(float[] data) {
        return kinect?KinectMeasureConstants.MINIMA_FINDER_ALFA:MocapMeasureConstants.MINIMA_FINDER_ALFA;
    }

    private List<Integer> findInterestingLocalMaxima(float[] data, int alpha, double beta) {
        List<Integer> indices = new LinkedList<>();
        int alphaLimit;
        int maxIndex;
        double maxValue;
        int i = 0;
        while (i < data.length) {
            maxIndex = -1;
            maxValue = Double.MIN_VALUE;
            while (i < data.length && data[i] < beta) {
                ++i;
            }
            alphaLimit = i + alpha;
            while (i < alphaLimit && i < data.length) {
                if (data[i] > maxValue) {
                    maxIndex = i;
                    maxValue = data[i];
                    alphaLimit = maxIndex + alpha;
                }
                ++i;
            }
            if (maxIndex > -1) {
                indices.add(maxIndex);
            }
        }

        return indices;
    }

    @Override
    public List<Integer> findExtremes(float[] data) {
        double beta = chooseBeta(data);
        int alpha = chooseAlpha(data);
        List<Integer> maxima = findInterestingLocalMaxima(data, alpha, beta);
        return maxima;
    }
}
